module rlog

go 1.15

require github.com/astaxie/beego v1.12.1

require (
	github.com/mfonda/simhash v0.0.0-20151007195837-79f94a1100d6
	github.com/shiena/ansicolor v0.0.0-20200904210342-c7312218db18 // indirect
	github.com/smartystreets/goconvey v1.6.4
	golang.org/x/crypto v0.0.0-20191205180655-e7c4368fe9dd // indirect
	golang.org/x/net v0.0.0-20200324143707-d3edc9973b7e // indirect
	golang.org/x/text v0.3.2 // indirect
	gorm.io/driver/clickhouse v0.1.0
	gorm.io/gorm v1.20.8
)
