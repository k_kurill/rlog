package main

import (
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/plugins/auth"
	"gorm.io/driver/clickhouse"
	"gorm.io/gorm"
	"rlog/controllers"
	"rlog/models"
)

func init() {
	dsn := "tcp://" + beego.AppConfig.String("ClickHouseHost") +
		":" + beego.AppConfig.String("ClickHousePort") +
		"?database=" + beego.AppConfig.String("ClickHouseDatabase") +
		"&username=" + beego.AppConfig.String("ClickHouseUser") +
		"&password=" + beego.AppConfig.String("ClickHousePassword") +
		"&read_timeout=10&write_timeout=20"
	db, err := gorm.Open(clickhouse.New(clickhouse.Config{
		DSN: dsn,
	}), &gorm.Config{})
	models.Db = db
	db.Exec("TRUNCATE TABLE IF EXISTS projects")
	db.AutoMigrate(&models.Project{})
	db.Set("gorm:table_options", "ENGINE=MergeTree() ORDER BY (project_id, created_at)").
		AutoMigrate(&models.Log{})
	new(models.Project).FillProjectTable()
	if err != nil {
		panic(err)
	}
}

func main() {
	beego.InsertFilter("/*", beego.BeforeRouter, auth.Basic(
		beego.AppConfig.String("BasicAuthLogin"),
		beego.AppConfig.String("BasicAuthPassword"),
	))
	beego.ErrorController(&controllers.ErrorController{})
	beego.Router("/", &controllers.LogController{})
	beego.Router("/list", &controllers.LogListController{})
	beego.Run()
}
