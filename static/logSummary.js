let newTotal = 0
let favicon = new Favico({
  animation:'none'
});
favicon.badge(newTotal)
$(document).ready(function() {
  let url = new URL(window.location);
  let filterParams = new URLSearchParams(url.search.slice(1));
  let headerSearch = $('#headerSearch')

  if(filterParams.get('Search')) {
    headerSearch.val(filterParams.get('Search'))
  }

  initDateRangePicker(filterParams)

  $('#datefilter').on('hide.daterangepicker', function(ev, picker) {
    updateDateRangePicker(filterParams, picker)
  });

  $('#datefilter-clear').on('click', function () {
    let $picker = $('#datefilter')
    let picker = $picker.data('daterangepicker')
    $picker.val('')
    updateDateRangePicker(filterParams, picker)
  })

  $('#datefilter-find').on('click', function () {
    let $picker = $('#datefilter')
    let picker = $picker.data('daterangepicker')
    picker.setStartDate(moment().format('DD.MM.YYYY'))
    picker.setEndDate(moment().format('DD.MM.YYYY'))
    updateDateRangePicker(filterParams, picker)
  })

  headerSearch.on('change', function (){
    if($(this).val()) {
      filterParams.set('Search', $(this).val())
    } else {
      filterParams.delete('Search')
    }

    reloadList(filterParams, false)
  })

  $('#headerSearchForm').on('submit', function (e){
    e.preventDefault()
    headerSearch.trigger('change')
  })

  moment.locale('ru')
  makeDatesRelative()
  if (filterParams.get('p') <= 1) {
    setInterval(function () {
      reloadList(filterParams, true)
    }, 15 * 1000)
    setInterval(function () {badge(newTotal)},1000)
  }
});

function reloadList(filterParams, highlight) {
  let url = new URL(window.location);
  $(".navbar li.nav-item>a").each(function () {
    let href = new URL($(this).attr('href'), url);
    let linkParams = new URLSearchParams(href.search.slice(1));
    filterParams.delete('ProjectId')
    filterParams.set('ProjectId', linkParams.get('ProjectId'))
    if (!highlight) {
      filterParams.delete('p')
    }
    history.pushState({}, "", url.pathname + "?" + filterParams.toString())
    $(this).attr('href', href.pathname + "?" + filterParams.toString() )
  })

  let errorsCountArray = rememberCounts()
  $("#listContainer").load(window.location + " #errorsList", function () {
    makeDatesRelative()
    if(highlight) {
      highlightChanged(errorsCountArray)
    }
  })
}

function badge(count) {
  favicon.badge(count)
}

function initDateRangePicker(filterParams) {
  let config = {
    locale: {
      format: 'DD.MM.YYYY',
      separator: ' - ',
      applyLabel: 'Применить',
      cancelLabel: 'Отменить',
      fromLabel: 'От',
      toLabel: 'До',
      monthNames: ['Январь','Февраль','Март','Апрель','Май','Июнь',
        'Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'],
      daysOfWeek: ['Вс','Пн','Вт','Ср','Чт','Пт','Сб'],
      firstDay: 1
    }
  }
  let isClear = true
  if (filterParams.get('StartDate') && filterParams.get('EndDate')) {
      config.startDate = filterParams.get('StartDate')
      config.endDate = filterParams.get('EndDate')
      isClear = false
  }
  let $datefilter = $('#datefilter')
  $datefilter.daterangepicker(config)
  if(isClear) {
    $datefilter.val('')
  }
}

function updateDateRangePicker(filterParams, picker) {
  if (!$('#datefilter').val()) {
    filterParams.delete('EndDate')
    filterParams.delete('StartDate')
  } else {
    filterParams.set('EndDate', picker.endDate.format('DD.MM.YYYY'))
    filterParams.set('StartDate', picker.startDate.format('DD.MM.YYYY'))
  }
  reloadList(filterParams, false)
}

function makeDatesRelative() {
  $('h3.date-created').each(function () {
    $(this).text(moment($(this).text(), "DD.MM.YYYY hh:mm:ss").fromNow())
    $(this).removeClass('d-none')
  })
}

function rememberCounts() {
  let errorsCountArray = new Map()
  $('.error-card').each(function () {
    errorsCountArray.set($(this).data('id'), $(this).data('count'))
  })
  return errorsCountArray
}

function highlightChanged(errorsCountArray) {
  let newCount = rememberCounts()
  newCount.forEach(function (count, id ) {
    if(errorsCountArray.get(id) !== count) {
      let $card = $('.error-card[data-id='+id+']').closest('.card')
      $card.addClass('card-danger')
      $card.removeClass('callout')
      if(errorsCountArray.get(id) > 0) {
        newTotal += (count - errorsCountArray.get(id))
      } else {
        newTotal++
      }

      setTimeout(function () {
        $card.removeClass('card-danger')
        $card.addClass('callout')
      }, 10 * 1000)
    }
  })
}

function setActiveMenu() {
  let url = new URL(window.location);
  $(".navbar li.nav-item>a").each(function () {
    let href = new URL($(this).attr('href'), url);
    if (url.pathname === href.pathname) {
      $(this).addClass('active')
    }
  })
}
