package routers

import (
	"rlog/controllers"
	"github.com/astaxie/beego"
)

func init() {
	beego.Router("/", &controllers.LogController{})
}
