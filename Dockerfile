FROM golang:1.15.5

WORKDIR /go/src/rlog
ADD . /go/src/rlog
RUN go get .
RUN go install .
ADD ./conf /go/bin/conf
ADD ./static /go/bin/static
ADD ./views /go/bin/views

ENTRYPOINT ["rlog"]
