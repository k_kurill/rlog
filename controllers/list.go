package controllers

import (
	"rlog/models"
	"github.com/astaxie/beego/utils/pagination"
	"gorm.io/gorm"
)

type LogListController struct {
	Common
}

func (c *LogListController) Get() {
	template := c.PrepareView()
	template.TplName = "list.html"
	var logs []models.Log

	logSearch := models.LogSearch{
		SearchString: c.GetString("Search"),
		StartDate:    c.GetString("StartDate"),
		EndDate:      c.GetString("EndDate"),
	}
	logSearch.ProjectId, _ = c.GetInt("ProjectId")
	var query *gorm.DB
	if groupId := c.GetString("GroupId"); len(groupId) > 0 {
		query = logSearch.BuildQuery().Where("group_id = ?", groupId)
	} else {
		query = logSearch.BuildQuery()
	}
	var err error
	logSearch.ProjectId, err = c.GetInt("ProjectId")
	if err != nil {
		logSearch.ProjectId = models.GetFirstProject()
	}
	var totalCount int64
	query.Count(&totalCount)
	paginator := pagination.SetPaginator(c.Ctx, models.PostsPerPage, totalCount)
	query.Limit(models.PostsPerPage).
		Offset(paginator.Offset()).
		Find(&logs)
	template.Data["paginator"] = paginator
	template.Data["Logs"] = logs
	template.Data["LogSearch"] = &logSearch
	template.Data["GroupCount"] = models.GroupCount(&logSearch)
	template.Data["LogsCount"] = models.LogsCount(&logSearch)
}
