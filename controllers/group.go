package controllers

import (
	"encoding/json"
	"github.com/astaxie/beego/utils/pagination"
	"rlog/models"
)

type LogMessage struct {
	ProjectId uint16
	Message   string
}

type LogController struct {
	Common
}

func (c *LogController) Get() {
	var logs []*models.Log
	template := c.PrepareView()
	logSearch := models.LogSearch{
		SearchString: c.GetString("Search"),
		StartDate:    c.GetString("StartDate"),
		EndDate:      c.GetString("EndDate"),
	}
	var err error
	logSearch.ProjectId, err = c.GetInt("ProjectId")
	if err != nil {
		logSearch.ProjectId = models.GetFirstProject()
	}
	query := logSearch.BuildGroupQuery()
	var totalCount int
	logSearch.GetGroupsCount(&totalCount)
	paginator := pagination.SetPaginator(c.Ctx, models.PostsPerPage, int64(totalCount))
	query.Offset(paginator.Offset()).Limit(models.PostsPerPage)
	query.Find(&logs)
	template.Data["paginator"] = paginator
	template.Data["Logs"] = logs
	template.Data["LogSearch"] = &logSearch
	template.Data["GroupCount"] = models.GroupCount(&logSearch)
	template.Data["LogsCount"] = models.LogsCount(&logSearch)

	c.TplName = "groups.html"
}

func (c *LogController) Post() {
	c.Ctx.Output.Header("Content-Type", "application/json")
	logMessage := new(LogMessage)
	err := json.Unmarshal(c.Ctx.Input.RequestBody, logMessage)

	if c.errorResponse(err) {
		return
	}

	log := new(models.Log)
	log.ProjectId = logMessage.ProjectId
	log.Message = logMessage.Message
	log.ProcessMessage()
	c.successResponse()
}

func (c *LogController) errorResponse(err error) bool {
	if err != nil {
		res, _ := json.Marshal(map[string]string{
			"status": err.Error(),
		})
		c.Ctx.WriteString(string(res))
		return true
	}
	return false
}

func (c *LogController) successResponse() {
	res, _ := json.Marshal(map[string]string{
		"status": "Ok",
	})
	c.Ctx.WriteString(string(res))
}
