package controllers

import (
	"rlog/models"
	"github.com/astaxie/beego"
)

type Common struct {
	beego.Controller
}

func (c *Common) PrepareView() *Common {
	c.Layout = "layouts/admin_template.html"
	c.LayoutSections = make(map[string]string)
	c.LayoutSections["HtmlHead"] = "layouts/header.html"
	c.LayoutSections["HtmlSidebar"] = "layouts/sidebar.html"
	c.LayoutSections["HtmlFooter"] = "layouts/footer.html"
	c.Data["Projects"] = new(models.Project).GetAllProjects()
	return c
}
