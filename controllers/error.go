package controllers

import "github.com/astaxie/beego"

type ErrorController struct {
	beego.Controller
}

func (c *ErrorController) Error404() {
	c.Layout = "layouts/admin_template.html"
	c.LayoutSections = make(map[string]string)
	c.LayoutSections["HtmlHead"] = "layouts/header.html"
	c.LayoutSections["HtmlSidebar"] = "layouts/sidebar.html"
	c.LayoutSections["HtmlFooter"] = "layouts/footer.html"
	c.TplName = "404.html"
}
