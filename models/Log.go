package models

import (
	"fmt"
	"github.com/mfonda/simhash"
	"hash/crc32"
	"regexp"
	"strconv"
	"strings"
	"time"
)

type Log struct {
	CreatedAt int64 `gorm:"autoUpdateTime:nano"`
	ProjectId uint16
	GroupId   uint32
	Message   string
	Simhash   string
}

func (l *Log) ProcessMessage() {
	l.Simhash = fmt.Sprintf("%x", simhash.Simhash(simhash.NewWordFeatureSet([]byte(l.Message))))
	l.groupMessage()
}

func (l *Log) groupMessage() {
	var logs []*Log
	Db.Distinct().Model(&Log{}).Select("simhash", "group_id").Where("project_id = ?", l.ProjectId).Find(&logs)
	curHash, _ := strconv.ParseUint(l.Simhash, 16, 64)
	var lHash uint64
	minDist := minHashDistance + 1
	for _, log := range logs {
		lHash, _ = strconv.ParseUint(log.Simhash, 16, 64)
		dist := simhash.Compare(curHash, lHash)
		if dist < minDist {
			minDist = dist
			l.GroupId = log.GroupId
		}
	}
	if l.GroupId == 0 {
		h := crc32.NewIEEE()
		h.Write([]byte(l.Message))
		l.GroupId = h.Sum32()
	}
	Db.Create(&l)
}

func (l *Log) GroupTitle() string {
	replaceRegexp := regexp.MustCompile(".*]")
	message := replaceRegexp.ReplaceAllString(l.Message, "")
	return message[:strings.Index(message, "\n")]
}

func (l *Log) CountInGroup(search *LogSearch) int64 {
	var count int64
	search.BuildQuery().Where("group_id = ?", l.GroupId).Count(&count)
	return count
}

func LogsCount(search *LogSearch) int64 {
	var count int64
	Db.Model(&Log{}).Where("`project_id` = ?", search.ProjectId).Count(&count)
	return count
}

func GroupCount(search *LogSearch) int64 {
	var count int64
	Db.Model(&Log{}).Select("group_id").Where("`project_id` = ?", search.ProjectId).Distinct().Count(&count)
	return count
}

func (l *Log) DateCreate() string {
	tm := time.Unix(0, l.CreatedAt)
	location, err := time.LoadLocation("Europe/Kiev")
	if err == nil {
		tm = tm.In(location)
	}
	return tm.Format("02.01.2006 15:04:05")
}
