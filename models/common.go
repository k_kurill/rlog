package models

import "gorm.io/gorm"

var Db *gorm.DB

const minHashDistance = uint8(6)
const PostsPerPage = 50
