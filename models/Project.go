package models

import (
	"encoding/json"
	"log"
	"os"
)

type Project struct {
	Id          int
	AwesomeIcon string
	Title       string
}

func (p *Project) FillProjectTable() {
	projects := p.loadProjects()
	if len(projects) > 0 {
		Db.Create(p.loadProjects())
	} else {
		log.Fatal("Have no projects in project.json file")
	}
}

func (p *Project) GetAllProjects() *[]Project {
	var projects []Project
	Db.Find(&projects)
	return &projects
}

func GetFirstProject() int {
	var project Project
	Db.First(&project)
	return project.Id
}

func (p *Project) loadProjects() []Project {
	projectsFile, err := os.Open("conf/projects.json")
	if err != nil {
		log.Println(err)
		return []Project{}
	}
	defer projectsFile.Close()
	var decoder = json.NewDecoder(projectsFile)
	var projects []Project
	err = decoder.Decode(&projects)
	if err != nil {
		log.Println(err)
		return []Project{}
	}
	return projects
}
