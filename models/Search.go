package models

import (
	"fmt"
	"gorm.io/gorm"
	"net/url"
	"reflect"
	"time"
)

type LogSearch struct {
	SearchString string
	StartDate    string
	EndDate      string
	ProjectId    int
}

func (ls *LogSearch) GetGroupsCount(totalCount *int) {
	query := Db.Select("count(DISTINCT(group_id)) as count").
		Model(&Log{}).
		Where("project_id = ?", ls.ProjectId)
	if len(ls.SearchString) > 0 {
		query.Where("message LIKE ?", "%"+ls.SearchString+"%")
	}
	if len(ls.StartDate) > 0 && len(ls.EndDate) > 0 {
		location, err := time.LoadLocation("Europe/Kiev")
		startTime, err := time.ParseInLocation("02.01.2006", ls.StartDate, location)
		endTime, err2 := time.ParseInLocation("02.01.2006 15:04:05", ls.EndDate+" 23:59:59", location)
		if err == nil && err2 == nil {
			query.Where("created_at BETWEEN ? AND ?", startTime.UnixNano(), endTime.UnixNano())
		}
	}
	query.Scan(totalCount)

	/*stmt := query.Session(&gorm.Session{DryRun: true}).Find(&[]Log{}).Statement
	fmt.Println(stmt.SQL.String() )*/
}

func (ls *LogSearch) BuildGroupQuery() *gorm.DB {
	subQuery := Db.
		Select("group_id", "max(created_at) as created_at").
		Distinct().
		Model(&Log{}).
		Where("project_id = ?", ls.ProjectId).
		Group("group_id")
	if len(ls.SearchString) > 0 {
		subQuery.Where("message LIKE ?", "%"+ls.SearchString+"%")
	}

	query := Db.
		Model(&Log{}).
		Select("message", "created_at", "group_id").
		Order("created_at desc").
		Joins("INNER JOIN (?) AS sub ON sub.group_id = logs.group_id AND logs.created_at = sub.created_at", subQuery)
	if len(ls.StartDate) > 0 && len(ls.EndDate) > 0 {
		location, err := time.LoadLocation("Europe/Kiev")
		startTime, err := time.ParseInLocation("02.01.2006", ls.StartDate, location)
		endTime, err2 := time.ParseInLocation("02.01.2006 15:04:05", ls.EndDate+" 23:59:59", location)
		if err == nil && err2 == nil {
			query.Where("created_at BETWEEN ? AND ?", startTime.UnixNano(), endTime.UnixNano())
		}
	}
	return query
}

func (ls *LogSearch) BuildQuery() *gorm.DB {
	query := Db.
		Model(&Log{}).
		Where("project_id = ?", ls.ProjectId).
		Order("created_at desc")
	if len(ls.SearchString) > 0 {
		query.Where("message LIKE ?", "%"+ls.SearchString+"%")
	}
	if len(ls.StartDate) > 0 && len(ls.EndDate) > 0 {
		startTime, err := time.Parse("02.01.2006", ls.StartDate)
		endTime, err2 := time.Parse("02.01.2006 15:04:05", ls.EndDate+" 23:59:59")
		if err == nil && err2 == nil {
			query.Where("created_at BETWEEN ? AND ?", startTime.UnixNano(), endTime.UnixNano())
		}
	}
	return query
}

func (ls *LogSearch) BuildQueryString() string {
	values := url.Values{}
	iVal := reflect.ValueOf(ls).Elem()
	typ := iVal.Type()
	for i := 0; i < iVal.NumField(); i++ {
		switch {
		case typ.Field(i).Type.Name() == "int" && iVal.Field(i).Int() == 0:
			continue
		case typ.Field(i).Type.Name() == "string" && iVal.Field(i).String() == "":
			continue
		}

		values.Set(typ.Field(i).Name, fmt.Sprint(iVal.Field(i)))
	}
	if str := values.Encode(); len(str) > 0 {
		return "?" + str
	}
	return ""
}
